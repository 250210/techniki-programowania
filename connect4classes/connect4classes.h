#ifndef CONNECT4CLASSES_H_INCLUDED
#define CONNECT4CLASSES_H_INCLUDED

enum pole{ CROSS, CIRCLE, NONE};
const char* pole_names[] = {" [X] "," [O] "," [ ] "};

class Game
{
public:
    Game(int, int);
    void Picking();
    void Gameplay();
    bool IsWon();
    void SetCounter(int);
    bool Countdown();
    void Show();
    void SwapPlayer();
    int GetWidth();
    int GetHeight();

private:
    pole** tab;
    int counter;
    pole player;
    int width,height;
};

#endif // CONNECT4CLASSES_H_INCLUDED
