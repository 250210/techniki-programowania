#include <iostream>
#include "connect4classes.h"
using namespace std;


void Game::Picking()
{int spot, p, x;
    do{
            do{
            cout << "Pick a non-empty spot (1-" << GetHeight() <<  "): ";
            cin >> spot;
            }while(spot>GetHeight() || spot<1);
            x=(spot-1);
            p=0;
            for(int h=1; h<=GetWidth(); h++)
            if(tab[GetWidth()-h][x]==NONE){
            tab[GetWidth()-h][x]=player;
            p=1;
            break;}
            if(p!=1) cout << "This slot is taken" << endl;

        }while(p!=1);


}

void Game::Gameplay()
{

do
    {
        cout << endl;
        SwapPlayer();
        cout << "PLAYER - " << pole_names[player];
        Picking();
        Show();
    }while(IsWon()==0&&Countdown()==0);
}

void Game::Show()
{
{
    for(int k=0; k<GetHeight(); k++)
    {
        cout << "  " << k+1 << "  ";
    }
    cout << endl;
    for (int i=0; i<GetWidth(); i++)
    {
        for(int j=0; j<GetHeight(); j++)
            cout << pole_names[tab[i][j]];
    cout << endl;}
return;}
}


int main()
{
    int n,m;
    cout << "Podaj 1. wymiar planszy: ";
    cin >> n;
    cout << "Podaj 2. wymiar planszy: ";
    cin >> m;
    Game Plansza(n,m);
    Plansza.Show();
    Plansza.SetCounter(m*n);
    Plansza.Gameplay();

return 0;}
