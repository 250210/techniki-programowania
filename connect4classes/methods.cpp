#include <iostream>
#include "connect4classes.h"
using namespace std;

Game::Game(int n, int m)
{   if(n<4) n=4;
    if(m<4) m=4;
    width = n;
    height = m;
    tab = new pole*[n];

    for(int i=0; i<n; i++)
    {
        tab[i] = new pole[m];
    }

    for (int i=0; i<n; i++)
    {
        for (int j=0; j<m; j++)
                tab[i][j] = NONE;

    }
}

int Game::GetHeight()
{
    return height;
}
int Game::GetWidth()
{
    return width;
}

bool Game::IsWon()
{
    int n=GetWidth();
    int m=GetHeight();

    for(int i=n-1; i>=0; i--)
    {
        for(int j=m-1; j>=0; j--)
        {
            if(tab[i][j]!=NONE){
                if((j-3)>=0) { if((tab[i][j]==tab[i][j-1]) && (tab[i][j-1]==tab[i][j-2]) && (tab[i][j-2]==tab[i][j-3])) {cout << pole_names[tab[i][j]] << " WON"; return 1;}}
                if((i-3)>=0) { if((tab[i][j]==tab[i-1][j]) && (tab[i-1][j]==tab[i-2][j]) && (tab[i-2][j]==tab[i-3][j])) {cout << pole_names[tab[i][j]] << " WON"; return 1;}}
                if(((j-3)>=0)&&((i-3)>=0)) { if((tab[i][j]==tab[i-1][j-1]) && (tab[i-1][j-1]==tab[i-2][j-2]) && (tab[i-2][j-2]==tab[i-3][j-3])) {cout << pole_names[tab[i][j]] << " WON"; return 1;}}
                if(((j+3)<m)&&((i-3)>=0)) { if((tab[i][j]==tab[i-1][j+1]) && (tab[i-1][j+1]==tab[i-2][j+2]) && (tab[i-2][j+2]==tab[i-3][j+3])) {cout << pole_names[tab[i][j]] << " WON"; return 1;}} }
        }
    }
return 0;}

void Game::SetCounter(int c)
{
    counter =c;
}
bool Game::Countdown()
{
    counter--;
    if(counter==0) return 1;
    else return 0;
}
void Game::SwapPlayer()
{
    if(player==CROSS) player=CIRCLE;
    else player=CROSS;
}
