#ifndef COMPLEX_H_INCLUDED
#define COMPLEX_H_INCLUDED

#include <iostream>
using std::ostream;

class Complex{

    public:
        //konstruktory
        Complex();
        Complex(double r);
        Complex(double r, double i);

        //dodawanie
        Complex operator+(const Complex& ir) const;
        Complex operator+(double r) const;
        friend Complex operator+(double r, const Complex& ir);

        //odejmowanie
        Complex operator-(const Complex& ir) const;
        Complex operator-(double r) const;
        friend Complex operator-(double r, const Complex& ir);

        //mno�enie
        Complex operator*(const Complex& ir) const;
        Complex operator*(double r) const;

        //wypisanie
        friend ostream& operator << (ostream&,const Complex&);


    private:
        double _re;
        double _im;

};

#endif // COMPLEX_H_INCLUDED
