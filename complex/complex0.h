#ifndef COMPLEX_H_INCLUDED
#define COMPLEX_H_INCLUDED

#include <iostream>
using std::ostream;

class Complex{

    public:
        //konstruktory
        Complex();
        Complex(double r);
        Complex(double r, double i);

        //dodawanie
        Complex operator+(const Complex&) const;
        Complex operator+(double ) const;
        friend Complex operator+(double , const Complex&);

        //odejmowanie
        Complex operator-(const Complex&) const;
        Complex operator-(double) const;
        friend Complex operator-(double, const Complex&);

        //mnozenie
        Complex operator*(const Complex& ir) const;
        Complex operator*(double r) const;
        friend Complex operator*(double, const Complex&);

        //dzielenie
        Complex operator/(const Complex&) const;
        Complex operator/(double) const;
        friend Complex operator/(double,const Complex&);

        //minus unitarny
        Complex operator-();

        double Length() const;
        double Phi() const;

        Complex Power(int n) const;

        Complex Conjugate() const;


        friend ostream& operator << (ostream&,const Complex&);


    private:
        double _re;
        double _im;

};

#endif // COMPLEX_H_INCLUDED
