#include <iostream>
#include "complex0.h"
#include <math.h>


using std::ostream;

Complex::Complex()
{
    _re=0;
    _im=0;
}

Complex::Complex(double r)
{
    _re=r;
    _im=0;
}
Complex::Complex(double r, double i)
{
    _re=r;
    _im=i;
}



Complex Complex::operator+(const Complex& add) const
{
    Complex result(_re + add._re,_im + add._im);
    return result;
}

Complex Complex::operator+(double r) const
{

    Complex result(_re+r,_im);
    return result;
}

Complex operator+(double r, const Complex& add)
{
    Complex result(r+add._re , add._im);
    return result;
}

Complex Complex::operator-(const Complex& sub) const
{
    Complex result(_re - sub._re , _im - sub._im);
    return result;
}

Complex Complex::operator-(double r) const
{

    Complex result(_re-r,_im);
    return result;
}


Complex operator-(double r, const Complex& sub)
{
    Complex result(r-sub._re , -(sub._im));
    return result;
}

Complex Complex::operator*(const Complex& factor) const
{
    Complex result(_re*factor._re - _im*factor._im , _re*factor._im+_im*factor._re);
    return result;
}

Complex Complex::operator*(double r) const
{
    Complex result(_re*r , _im*r);
    return result;
}

Complex operator*(double r, const Complex& factor)
{
    Complex result(factor._re*r , factor._im*r);
    return result;
}

Complex Complex::operator/(const Complex& div) const
{
    Complex res(_re,_im);
    res = res*div.Conjugate();
    double d = div.Length()*div.Length();
    res = res/d;
    return res;
}
Complex Complex::operator/(double r) const
{
    Complex res(_re/r, _im/r);
    return res;
}

Complex operator/(double r,const Complex& div)
{
    Complex result(r);
    result=result/div;
    return result;
}
Complex Complex::Conjugate() const
{
    Complex result(_re, -(_im));
    return result;
}

Complex Complex::operator-()
{
    Complex result(-_re,-_im);
    return result;
}
ostream& operator << (ostream& out,const Complex& ir)
{
    char c,d;
    if(ir._im<0) c='-';
    else c='+';
    if(ir._re<0) d='-';
    else d=' ';
    out <<d<<fabs(ir._re) << " " <<c<<" "<< fabs(ir._im) <<"i"<< std::endl;
    return out;
}


double Complex::Length() const{
return sqrt(_re*_re + _im*_im);
}

double Complex::Phi() const{
if(_re>0) return atan(_im/_re);
else if(_re<0) return atan(_im/_re)+M_PI;
else if(_im>0) return (M_PI)/2;
else if(_im<0) return -M_PI/2;
else return 0;
}


Complex Complex::Power(int n) const{
 if(n<=0) return Complex(0,0);
 if(_re==0) return Complex(0,pow(_im,n)*pow(-1,n+1));
 else if(_im==0) return Complex(pow(_re,n), 0);

 double new_l = pow(Length(),n);
 double r = cos(n*Phi())*new_l;
 double i = sin(n*Phi())*new_l;
 Complex result(r,i);
 return result;

}
