#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include <iostream>
using std::ostream;

class VectorInt{

public:
    VectorInt();
    VectorInt(int);
    VectorInt(const VectorInt&);
    ~VectorInt();
    double At(int) const;
    void Insert(int, double);
    void PushBack(double);
    void PopBack();
    void ShrinkToFit();
    void Clear();
    int Size() const;
    int Capacity() const;
    friend ostream& operator << (ostream&,const VectorInt&);

private:
    void Resize(int);
    double* sequence;
    int cap;
    int current_size;

};

#endif // VECTOR_H_INCLUDED
