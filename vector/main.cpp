#include <iostream>
#include <stdlib.h>
#include "vector.h"


int main()
{
    using std::cout;
    using std::endl;
    VectorInt Vector;
    Vector.PushBack(8.91);
    Vector.PushBack(11);
    Vector.PushBack(15);
    Vector.Insert(11, 6.78);
    Vector.Insert(9, 7.68);
    Vector.Insert(2, 12);
    cout << Vector.Size() << ": " << Vector.Capacity() << endl;
    Vector.ShrinkToFit();
    cout << Vector.Size() << ": " << Vector.Capacity() << endl;
    for(int i=0; i<16; i++)
    {
        Vector.PushBack(2);
    }
    cout << Vector.Size() << ": " << Vector.Capacity() << endl;
    Vector.PopBack();
    Vector.PopBack();
    cout << Vector.Size() << ": " << Vector.Capacity() << endl;
    Vector.ShrinkToFit();
    cout << Vector.Size() << ": " << Vector.Capacity() << endl;
    cout << "Sequence: "<< endl << Vector;

return 0;

}
