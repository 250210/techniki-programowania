#include <iostream>
#include "vector.h"

VectorInt::VectorInt()
{
    this->cap = 16;
    this->current_size=0;
    this->sequence = new double[16];
    for(int i=0; i<16; i++)
    {
        this->sequence[i]=101.101;
    }
}

VectorInt::VectorInt(int c)
{
    this->cap = c;
    this->current_size=0;
    this->sequence = new double[c];
    for(int i=0; i<c; i++)
    {
        this->sequence[i]=101.101;
    }
}

VectorInt::VectorInt(const VectorInt& Pattern)
{
    cap = Pattern.Capacity();
    current_size = Pattern.Size();
    sequence = new double[cap];
    for(int i=0; i<cap; i++)
    {
        sequence[i]=Pattern.At(i);
    }
}
VectorInt::~VectorInt()
{
    delete[] sequence;
    std::cout << std::endl << "deleted" << std::endl;
}
int VectorInt::Capacity() const
{
    return this->cap;
}
int VectorInt::Size() const
{
    return this->current_size;
}
double VectorInt::At(int i) const
{
    return this->sequence[i];
}
void VectorInt::Insert(int i, double value)
{
    if(At(i)==101.101) PushBack(value);
    else sequence[i]=value;
}

void VectorInt::PushBack(double value)
{
    int s = Size();
    if(s<Capacity()) sequence[s]=value;
    else
    Resize(s+1);
    sequence[s]=value;
    current_size++;
}
void VectorInt::PopBack()
{
    sequence[Size()-1]=101.101;
    current_size--;
}
void VectorInt::ShrinkToFit()
{
    int new_capacity = Size();
    Resize(new_capacity);
}
void VectorInt::Resize(int new_size)
{
    int old_size = Capacity();
    double* old_sequence = new double[old_size];
    for(int i=0; i<old_size; i++)
    {
        old_sequence[i]=sequence[i];
    }
    delete[] sequence;
    sequence = new double[new_size];
    {
        for(int j=0; j<new_size; j++)
        {
            if(j<old_size) sequence[j]=old_sequence[j];
            else sequence[j]=101.101;
        }

    }
    delete[] old_sequence;
    cap = new_size;
}
ostream& operator<<(ostream& out,const VectorInt& _vector)
{
    int _size = _vector.Size();
    for(int i=0; i<_size; i++)
    {
        out << _vector.At(i) << ";";
    }
    return out;
}


