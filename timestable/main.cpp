#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int n;
    cout << "Podaj rozmiar: ";
    cin >> n;
    cout << endl;

    int** tab = new int*[n];

    for(int i=0; i<n; i++)
    {
         tab[i] = new int[n];
    }


    for(int k=0; k<n; k++)
    {
        for(int l=0; l<n; l++)
        {
            tab[k][l] = (k+1)*(l+1);
        }
    }

        for(int k=0; k<n; k++)
    {
        for(int l=0; l<n; l++)
        {
            cout << setw(6) << tab[k][l];
        }
    cout << endl;
    }

for(int i=0; i<n; i++)
{
    delete[] tab[i];
}

delete[] tab;
return 0;}
