#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED
enum pole{ CROSS, CIRCLE, NONE};
const char* pole_names[] = {" [X] "," [O] "," [ ] "};

bool is_won(pole*[], int, int);
void initialize(pole*[], int, int);
void show(pole*[], int, int);

class Game
{
public:
    Game(int, int);
    void Picking();
    void Gameplay();
    bool IsWon();
    void SetCounter(int);
    bool Countdown();
    void Show();
    void SwapPlayer();
    int GetWidth();
    int GetHeight();

private:
    pole** tab;
    int counter;
    pole player;
    int width,height;
};
#endif // HEADER_H_INCLUDED
