/*main.cpp*/
/*Program demonstruj�cy mo�liwo�ci
klasy Point2d.*/
#include <iostream>
#include "point2d.h"
#include<cmath>
#define M_PI 4*atan(1.)

using namespace std;

int main()
{
    //obiekt a utworzony z wykorzystaniem konstruktura domy�lnego
    Point2d a;
    //do wy�wietlania wykorzystujemy przeci��ony operator <<
    //std::ostream& operator<<(std::ostream& out, Point2d& p);
    cout << "a = " << a << endl;
    //metoda setXY ustawia wsp�rz�dne punktu
    a.setXY(3, 4);
    cout << "a = " << a << endl;

    //obiekt b utworzony z wykorzystaniem konstruktora pobieraj�cego
    //dwie liczby - wsp�rz�dne punktu
    Point2d b(-2,-1);
    cout << "b = " << b << endl;

    //obiekt c utworzony z wykorzystaniem konstruktora kopiuj�cego
    Point2d c(b);
    cout << "c = " << c << endl;

    //wykorzystanie metod get do odczytania wsp�rz�dnych punktu
    cout << "b.x = "   << b.getX() << endl;
    cout << "b.y = "   << b.getY() << endl;
    cout << "b.r = "   << b.getR() << endl;
    cout << "b.phi = " << b.getPhi() << endl;

    //metoda setRPhi ustawia wsp�rz�dne punktu na podstawie
    //wsp�rz�dnych biegunowych
    //b.setRPhi(5,1);
    //cout << "b = " << b << endl;

    b.rotation(M_PI);
    b.homothety(-2);

    cout << "b.x = "   << b.getX() << endl;
    cout << "b.y = "   << b.getY() << endl;
    cout << "b.r = "   << b.getR() << endl;
    cout << "b.phi = " << b.getPhi() << endl;


    return 0;
}

