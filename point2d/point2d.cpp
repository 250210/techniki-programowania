
#include"point2d.h"
#include<iostream>
#include<cmath>
#define M_PI 4*atan(1.)


//konstruktor bez argument�w zwraca punkt (0,0)
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d()
    : _r(0)
    , _phi(0){
}

//konstruktor pobieraj�cy wsp�rz�dne
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d(double x, double y)
    : _r(sqrt(x*x + y*y))
    , _phi(atan2(y,x)){
}

//konstruktor kopiuj�cy
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d(const Point2d& other)
    : _r(other._r)
    , _phi(other._phi){
}

//przeci��ony operator przypisania
Point2d& Point2d::operator= (const Point2d& other){
    //wykorzystano wska�nik this pokazuj�cy
    //na "ten" obiekt
    this->_r = other._r;
    this->_phi = other._phi;
    //operator zwraca "ten" obiekt, aby mo�na by�o
    //wykona� wielokrotne przypisanie
    return *this;
}

//wsp�rz�dna x
double Point2d::getX(){
    return _r*cos(_phi);
}

//wsp�rz�dna y
double Point2d::getY(){
    return _r*sin(_phi);
}

//wsp�rz�dna r
double Point2d::getR(){
    return _r;
}

//wsp�rz�dna phi
double Point2d::getPhi(){
    return _phi;
}

void Point2d::setXY(double x, double y){
    _r=(sqrt(x*x + y*y));
    _phi=(atan2(y,x));
}

void Point2d::setRPhi(double r, double phi){
    _r=r;
    _phi = phi;
}

void Point2d::homothety(double k){
    double new_x = k*getX();
    double new_y = k*getY();
    setXY(new_x, new_y);
}

void Point2d::rotation(double phi){
    double r = getR();
    double new_phi = getPhi() + phi;
    setRPhi(r,new_phi);
    }

//przeci��ony operator<< dla wypisywania
std::ostream& operator<<(std::ostream& out, Point2d& p){
    return out << "[" << p.getX() << ", " << p.getY() << "]";
}
