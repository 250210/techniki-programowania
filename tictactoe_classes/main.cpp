#include <iostream>
#include "tictactoe_classes.h"

using namespace std;

int Game::BoardValue(int depth)
{
    int temp;
    int score;
    if(IsWon()==0)
    {
        PlayerSwap();
        temp=ChooseByAI();
        Put(temp);
        score=BoardValue(depth+1);
        Remove(temp);
        PlayerSwap();
        return score;
    }

    else if(IsWon()==2) {return 10-depth;}
    else if(IsWon()==-2) {return depth-10;}
    else {return 0;}
    }

int Game::ChooseByAI()
{       int depth=0;
        int score;
        if(GetPlayer()==CROSS) score = -11;
        else score = 11;
        int choice;
        int current;

        if(GetPlayer()==CROSS)
        {
                for(int i=1; i<=9; i++)
                {   if(Put(i)){
                    current = BoardValue(depth);
                    if(score<current)
                        {
                            score = current;
                            choice = i;
                        }
                    Remove(i);}}}

        else
        {   for(int i=1; i<=9; i++)
                {   if(Put(i))
                {
                    current=BoardValue(depth);
                    if(score>current)
                        {
                            score = current;
                            choice = i;
                        }
                    Remove(i);
                }}}
return choice;}


void Game::Empty()
{
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
            tab[i][j]=EMPTY;
    }
}
void Game::Show()
{
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<3; j++)
        cout << spot_names[tab[i][j]];

        cout << endl;
    }
}
int Game::IsWon()
{


    if((tab[0][0]!=EMPTY)&&(tab[0][0]==tab[1][1])&&(tab[1][1]==tab[2][2])) {if(tab[0][0]==CROSS)return 2;
                                                                            else return -2;}
    if((tab[2][0]!=EMPTY)&&(tab[2][0]==tab[1][1])&&(tab[1][1]==tab[0][2])) {if(tab[2][0]==CROSS)return 2;
                                                                            else return -2;}

    for(int i=0; i<3; i++)
    {
        if((tab[i][0]!=EMPTY)&&(tab[i][0]==tab[i][1])&&(tab[i][1]==tab[i][2])) {if(tab[i][0]==CROSS)return 2;
                                                                                else return -2;}
        if((tab[0][i]!=EMPTY)&&(tab[0][i]==tab[1][i])&&(tab[1][i]==tab[2][i])) {if(tab[0][i]==CROSS)return 2;
                                                                                else return -2;}
    }

    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
            if(tab[i][j]==EMPTY) return 0;
    }

return 1;

}
void Game::Remove(int a)
{

        int x = (a-1)/3;
        int y = (a-1)%3;
        tab[x][y]=EMPTY;

}
bool Game::Put(int a)
{
   if((a>9)||(a<1)) return 0;
   else
    {
        int x = (a-1)/3;
        int y = (a-1)%3;
        if(tab[x][y]==EMPTY) {tab[x][y]=GetPlayer(); return 1;}
        else return 0;
    }

}

void Game::PlayerSwap()
{
if(player==CROSS) player = CIRCLE;
else player = CROSS;
}
spot Game::GetPlayer()
{
    return this->player;
}

void Game::SetPlayer(spot which)
{
    player=which;
}
bool Game::IsOnAI()
{
    return ai;
}
void Game::UploadAI(bool p)
{
    this-> ai =p;
}

int main()
{

    Game Board;
    Board.SetPlayer(CROSS);
    Board.Empty();
    int a;
    bool p;
    cout << "Play against AI? (0/1)";
    cin >> p;
    Board.UploadAI(p);
    Board.Show();
    while(Board.IsWon()==0)
    {
        Board.PlayerSwap();

        if(Board.GetPlayer()==CROSS&&Board.IsOnAI()==1) {cout << "Outplayed by AI" << endl;
        Board.Put(Board.ChooseByAI());}
        else{
      do{  cout << "Player " << spot_names[Board.GetPlayer()] << ": Choose an emp spot (1-9): ";

        cin>>a;
        }while(Board.Put(a)==0);}

    Board.Show();
    }
       if(Board.IsWon()==2) cout << " X WON!";
        else if(Board.IsWon()==-2) cout << "O WON!";
        else cout << "DRAW";
}
