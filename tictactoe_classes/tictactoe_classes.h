#ifndef TICTACTOE_CLASSES_H_INCLUDED
#define TICTACTOE_CLASSES_H_INCLUDED

enum spot{CROSS,CIRCLE,EMPTY};
const char* spot_names[] = {" [X] ", " [O] ", " [ ] "};

class Game
{
public:
    void SetPlayer(spot);
    int ChooseByAI();
    bool Put(int);
    int IsWon();
    void Show();
    void Empty();
    int BoardValue(int);
    void Remove(int);
    void PlayerSwap();
    spot GetPlayer();
    bool IsOnAI();
    void UploadAI(bool);

    private:
    spot tab[3][3];
    spot player;
    bool ai;
};

#endif // TICTACTOE_CLASSES_H_INCLUDED
