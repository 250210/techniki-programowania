#include <iostream>
#include "figure.h"
#include <math.h>

Circle::Circle(double x):Figure()
{
if(x>0) r=x;
else r=1;
}

Triangle::Triangle(double x, double y, double z)
{

    if(x>0) a=x;
    else a=1;
    if(y>0) b=y;
    else b=1;
    if(z>0) c=z;
    else c=1;
}

Rectangle::Rectangle(double x, double y)
{
    if(x>0) a=x;
    else a=1;
    if(y>0) b=y;
    else b=1;
}

double Circle::area() const{
return (r*r*M_PI);
}

double Triangle::area() const{
return ((1/4)*sqrt((a+b+c)*(a+b-c)*(a-b+c)*(-a+b+c)));
}

double Rectangle::area() const{
return (a*b);
}


Regular::Regular(double x, int m)
{
    if(x>0) a=x;
    else a=1;

    if(m>2) n=m;
    else n=3;

}

double Triangle::in_radius() const{
double p = (a+b+c)/2;
return (sqrt((p-a)*(p-b)*(p-c))/sqrt(p));
}
double Triangle::on_radius() const{
double p= (a+b+c)/2;
return((a*b*c)/(4*p*in_radius()));

}

double Rectangle::in_radius() const{
if(a==b) return Regular(a,4).in_radius();
else return 0;}


double Rectangle::on_radius() const{
return(0.5*sqrt(a*a + b*b));
}
double Regular::in_radius() const{
return(a*(1/(2*tan(M_PI / n))));
}

double Regular::on_radius() const{
return(a*(1/(2*sin(M_PI / n))));
}

double Regular::area() const{
double p= 0.5 * n;
return(p*in_radius()*a);
}

std::ostream& Circle::print(std::ostream& out)const{
out <<"CIRCLE" << std::endl << "Radius: " << r << std::endl << "Area: " << std::endl << area() <<std::endl;
return out;
}

std::ostream& Regular::print(std::ostream& out)const{
out <<"REGULAR POLYGON" << std::endl << "Edge: " << a << std::endl << "Number of edges: " << n << std::endl << "Area: " << std::endl << area() <<std::endl;
return out;
}


std::ostream& Triangle::print(std::ostream& out)const{
out <<"TRIANGLE" << std::endl << "Dimensions: " << a << " x " << b << " x " << c << std::endl << "Area: " << std::endl << area()<<std::endl;
return out;
}

std::ostream& Rectangle::print(std::ostream& out)const{
out <<"RECTANGLE" << std::endl << "Dimensions: " << a << " x " << b << std::endl << "Area: " << std::endl << area()<<std::endl;
return out;
}

std::ostream& operator<<(std::ostream& out, const Figure& f)
{
    return f.print(std::cout);
}

