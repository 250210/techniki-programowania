#include <iostream>
#include "figure.h"

using namespace std;

int main()
{

    Figure** f = new Figure*[2];
    f[0]=new Triangle(2,2,2);
    f[1]=new Regular(2,3);
    cout << f[0]->in_radius() <<endl;
    cout << f[0]->on_radius() <<endl;
    cout << f[1]->in_radius() << endl;
    cout << f[1]->on_radius() << endl;
    cout << *f[0];
    cout << *f[1];
    return 0;
}
