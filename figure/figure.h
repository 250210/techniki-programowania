#ifndef FIGURE_H_INCLUDED
#define FIGURE_H_INCLUDED

class Figure{
    public:
        virtual double area() const=0;
        virtual double in_radius() const=0;
        virtual double on_radius() const=0;
        virtual std::ostream& print(std::ostream&) const=0;

        friend std::ostream& operator<<(std::ostream& out, const Figure& f);

};

class Circle : public Figure{

    public:
        Circle(double);
        virtual double area() const;
        virtual std::ostream& print(std::ostream&) const;

    private:
        double r;

};

class Triangle : public Figure{

    public:
        Triangle(double, double, double);
        virtual double in_radius() const;
        virtual double on_radius() const;
        virtual double area() const;
        virtual std::ostream& print(std::ostream&) const;

    private:
        double a;
        double b;
        double c;

};

class Rectangle : public Figure{

    public:
        Rectangle(double, double);
        virtual double area() const;
        virtual double in_radius() const;
        virtual double on_radius() const;
        virtual std::ostream& print(std::ostream&) const;

    private:
        double a;
        double b;

};

class Regular : public Figure{
    public:
        Regular(double, int);
        virtual double area() const;
        virtual std::ostream& print(std::ostream&) const;
        double in_radius() const;
        double on_radius() const;
    private:
        double a;
        int n;

};



#endif // FIGURE_H_INCLUDED
