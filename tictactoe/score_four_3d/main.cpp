#include <iostream>
#include <iomanip>
#include "header.h"
using namespace std;

void initialize(pole tab[4][4][4])
{
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
            for(int k=0; k<4; k++)
                tab[i][j][k] = NONE;
    }
return;}

void show(pole tab[4][4][4])
{
      for (int i=0; i<4; i++)
    {   cout << i+1 << ". layer" << endl;
        for (int j=0; j<4; j++)
        {
                for(int k=0; k<4; k++)
                {
                cout << pole_names[tab[i][j][k]];}
        cout << endl;}
        cout << endl << endl ;}
return;}



bool is_ended(pole tab[4][4][4])
{
    for(int i=0; i<4; i++)
    {
        if((tab[i][0][0]!=NONE)&&(tab[i][0][0]==tab[i][1][1])&&(tab[i][1][1]==tab[i][2][2])&&(tab[i][2][2]==tab[i][3][3])) {cout << endl << pole_names[tab[i][0][0]] << " WON!"; return 1;}
        if((tab[i][3][0]!=NONE)&&(tab[i][3][0]==tab[i][2][1])&&(tab[i][2][1]==tab[i][1][2])&&(tab[i][1][2]==tab[i][0][3])) {cout << endl << pole_names[tab[i][2][0]] << " WON!"; return 1;}
    }

    for(int j=0; j<4; j++)
    {

        for(int i=0; i<4; i++)
        {
            if((tab[j][i][0]!=NONE)&&(tab[j][i][0]==tab[j][i][1])&&(tab[j][i][1]==tab[j][i][2])&&(tab[j][i][3]==tab[j][i][3])) {cout << endl << pole_names[tab[j][i][0]] << " WON!"; return 1;}
            if((tab[j][0][i]!=NONE)&&(tab[j][0][i]==tab[j][1][i])&&(tab[j][1][i]==tab[j][2][i])&&(tab[j][2][i]==tab[j][3][i])) {cout << endl << pole_names[tab[j][0][i]] << " WON!"; return 1;}
        }}

        if((tab[0][0][0]!=NONE)&&(tab[0][0][0]==tab[1][1][1])&&(tab[1][1][1]==tab[2][2][2])&&(tab[2][2][2]==tab[3][3][3])) {cout << endl << pole_names[tab[3][3][3]] << " WON!"; return 1;}
        if((tab[3][0][0]!=NONE)&&(tab[3][0][0]==tab[2][1][1])&&(tab[2][1][1]==tab[1][2][2])&&(tab[1][2][2]==tab[0][3][3])) {cout << endl << pole_names[tab[0][3][3]] << " WON!"; return 1;}
        if((tab[0][3][0]!=NONE)&&(tab[0][3][0]==tab[1][2][1])&&(tab[1][2][1]==tab[2][1][2])&&(tab[2][1][2]==tab[3][0][3])) {cout << endl << pole_names[tab[3][0][3]] << " WON!"; return 1;}
        if((tab[3][3][0]!=NONE)&&(tab[3][3][0]==tab[2][2][1])&&(tab[2][2][1]==tab[1][1][2])&&(tab[1][1][2]==tab[0][0][3])) {cout << endl << pole_names[tab[0][0][3]] << " WON!"; return 1;}

        for(int i=0; i<4; i++)
        {
            if((tab[0][0][i]!=NONE)&&(tab[0][0][i]==tab[1][1][i])&&(tab[1][1][i]==tab[2][2][i])&&(tab[2][2][i]==tab[3][3][i])) {cout << endl << pole_names[tab[3][3][i]] << " WON!"; return 1;}
            if((tab[3][0][i]!=NONE)&&(tab[3][0][i]==tab[2][1][i])&&(tab[2][1][i]==tab[1][2][i])&&(tab[1][2][i]==tab[0][3][i])) {cout << endl << pole_names[tab[0][3][i]] << " WON!"; return 1;}

            if((tab[0][i][0]!=NONE)&&(tab[0][i][0]==tab[1][i][1])&&(tab[1][i][1]==tab[2][i][2])&&(tab[2][i][2]==tab[3][i][3])) {cout << endl << pole_names[tab[3][i][3]] << " WON!"; return 1;}
            if((tab[3][i][0]!=NONE)&&(tab[3][i][0]==tab[2][i][1])&&(tab[2][i][1]==tab[1][i][2])&&(tab[1][i][2]==tab[0][i][3])) {cout << endl << pole_names[tab[0][i][3]] << " WON!"; return 1;}
        }

        for(int i=0; i<4; i++)
        {
            for(int j=0; j<4; j++)
            {
                if((tab[0][i][j]!=NONE)&&(tab[0][i][j]==tab[1][i][j])&&(tab[1][i][j]==tab[2][i][j])&&(tab[2][i][j]==tab[3][i][j])) {cout << endl << pole_names[tab[0][i][j]] << " WON!"; return 1;}

        }}
return 0;}



int main(){
    int counter = 0;
    int p;
    int x,y;
    pole gra[4][4][4];
    initialize(gra);
    int spot;
    pole player=CROSS;
    show(gra);
    do
    {   if(counter>=64) {cout << "DRAW!!!"; break;}
        cout << endl;
        if(player==CROSS) player=CIRCLE;
        else player=CROSS;
        cout << "PLAYER - " << pole_names[player];

        do{
            do{
            cout << "Pick a non-empty spot (1-16): ";
            cin >> spot;
            }while(spot>16 || spot<1);
            x=(spot)/4;
            y=((spot)%4)-1;
            p=0;

            for(int h=0; h<4; h++)
{
             if(gra[h][x][y]==NONE){
            gra[h][x][y]=player;
            p=1;
            break;}
            if(p!=1) cout << "This slot is taken" << endl;}


        }while(p!=1);

        show(gra);
        counter++;
    }while(is_ended(gra)==0);



return 0;}

