#include <iostream>
#include <string>
#include "mytree.h"

using namespace std;


int main()
{
    string str = "Bartek";
    myTree<int,string> drzewo(5, str);
    drzewo.Add(6,"Krzysztof");
    drzewo.Add(18,"Natalia");
    drzewo.Add(4,"Kasia");
    drzewo.Add(7,"Kaja");
    drzewo.Add(9,"Hania");
    drzewo.Add(27,"Ania");
    drzewo.Remove(6);
    drzewo.Print();
    return 0;
}
