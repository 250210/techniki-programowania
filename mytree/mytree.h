#ifndef MYTREE_H_INCLUDED
#define MYTREE_H_INCLUDED

using namespace std;

template<typename K, typename V>
class myTree{

public:
    myTree(K,V);
    //myTree(&myTree<K,V>);
    void Add(K,V);
    void Remove(K);
    void Print();

private:
    myTree<K,V>* left;
    myTree<K,V>* right;
    K key;
    V value;

};
template<typename K,typename V>
myTree<K,V>::myTree(K new_key,V new_value){
key=new_key;
value=new_value;
left=NULL;
right=NULL;
};
/*template<typename K,typename V>
myTree<K,V>::myTree(const &myTree<K,V> Temp ){
key=Temp.key;
value=Temp.value;
left=Temp.left;
right=Temp.right;
};*/


template<typename K,typename V>
void myTree<K,V>::Add(K next_key, V next_value)
{
    if(next_key<key)
    {  if(left!=NULL)  (*left).Add(next_key,next_value);
       else left = new myTree<K,V>(next_key,next_value);
    }
    else if(next_key>key)
   {   if(right!=NULL)  (*right).Add(next_key,next_value);
       else right = new myTree<K,V>(next_key,next_value);

    }
    else if(next_key==key)
    {
        value=next_value;
    }

}
template<typename K,typename V>
void myTree<K,V>::Remove(K _key)
{
    if(left!=NULL&&(*left).key==_key)
    {
        myTree<K,V>* ptr = (*left).right;
        if(ptr!=NULL){
        while((*ptr).left!=NULL){ptr=(*ptr).left;}
        (*ptr).left=(*left).left;
        ptr = (*left).right;
        delete left;
        left=ptr; }
        else {
        ptr = (*left).left;
        delete left;
        left=ptr;
        }
    }
    else if(right!=NULL&&(*right).key==_key)
    {
        myTree<K,V>* ptr =(*right).right;
        if(ptr!=NULL){
        while((*ptr).left!=NULL){ptr=(*ptr).left;}
        (*ptr).left=(*right).left;
        ptr = (*right).right;
        delete right;
        right=ptr; }
        else {
        ptr= (*right).left;
        delete right;
        right=ptr;
        }
    }


    else  if(_key>key)
    {  if(right!=NULL) (*right).Remove(_key);
       else cout << "Nie ma takiego klucza"<<endl;}
    else if(_key<key)
    {  if(left!=NULL) (*left).Remove(_key);
       else cout << "Nie ma takiego klucza" << endl;}

}


template<typename K, typename V>
void myTree<K,V>::Print()
{
    if(left!=NULL) (*left).Print();
    cout << key << "::" << value << endl;
    if(right!=NULL) (*right).Print();
}




#endif // MYTREE_H_INCLUDED
