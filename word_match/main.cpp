#include <iostream>
#include <string.h>

using namespace std;
bool match(string s, string pattern)
{
    string new_s, new_pattern;
    int i=0;
    int k;
    do
    {
        if((s[i]!=pattern[i])&&(s[i]!='?'))
        {
            if(s[i]=='*')
            {   k=i;
                while(s[i]=='*')
                {
                    i++;
                }
                do{
                    if(s[i]=='?'||s[i]==pattern[k])
                        {
                        do{
                         new_s = s.substr(i, string::npos);
                         new_pattern = pattern.substr(k, string::npos);
                         k++;
                         }while((match(new_s, new_pattern)==0)&&(pattern[k-1]!='\0'));
                         return match(new_s, new_pattern);}
                k++;
                }while(pattern[k]!='\0');
                new_s = s.substr(i, string::npos);
                new_pattern = pattern.substr(k, string::npos);
                return match(new_s, new_pattern);


            }
            else return 0;}
        i++;
    }while(s[i-1]!='\0' && pattern[i-1]!='\0');
if(pattern[i-1]==s[i-1]) return 1;
else return 0;
}
int main()
{
    string s ="*?***bcd?f**????";
    string pattern ="abcdef";

    cout << match(s,pattern);

    return 0;
}
