#include <iostream>
#include <map>
#include <iterator>
#include <string>
#include <fstream>

using namespace std;

int main()
{
    string key,value;
    map<string, string> adressbook;
    ifstream plik;
    plik.open("adressbook.txt");
    if(plik.is_open())
    {
        string line;

        while(getline(plik, line))
        {
            int i=0;
            while(line[i]!=':') i++;
            key=line.substr(0,i);
            value=line.substr(i+1,string::npos);
           adressbook.insert(make_pair(key,value));
        }
    }

    plik.close();
    int selection;
    do{
    cout << "Choose action: " << endl;
    cout << "0. Show" << endl;
    cout << "1. Add new." << endl;
    cout << "2. Remove." << endl;
    cout << "3. Change." << endl;
    cout << "4. Delete all." << endl;
    cout << "5. Save and Exit." << endl;
    cout << "----------------" << endl;

    cin >> selection;
    switch(selection)
    {

     case 0:
     for(map<string,string>::iterator it = adressbook.begin(); it!=adressbook.end(); it++)
        {
            cout << it-> first << endl << it-> second << endl << "-------------------------------" << endl;

        }
        break;
    case 1:
        cout << "Podaj nazwe adresu: ";
        cin.ignore();
        getline(cin,key);
        cout << "Podaj adres: ";
        cin >> value;
        if(adressbook.find(key)==adressbook.end())
        adressbook.insert(make_pair(key,value));
        else cout << "Ta nazwa juz istnieje" << endl;
        break;
    case 2:
        cout << "Podaj nazwe adresu: ";
        cin.ignore();
        getline(cin, key);
        if(adressbook.find(key)==adressbook.end())
        cout << "Nie istnieje adres o takie nazwie"<<endl;
        else
        adressbook.erase(key);
        break;
    case 3:
        cout << "Podaj nazwe adresu: ";
        cin.ignore();
        getline(cin, key);
        cout << "Podaj nowy adres:" ;
        cin >> value;
        if(adressbook.find(key)==adressbook.end())
        cout << "Nie istnieje adres o takiej nazwie" <<endl;
        else
        adressbook[key]=value;
        break;
    case 4:
        adressbook.clear();
        break;
    case 5:
        ofstream plik_;
        plik_.open("adressbook.txt");
        for(map<string,string>::iterator i = adressbook.begin(); i!=adressbook.end(); i++)
        {
            plik_ << i-> first << ":" << i-> second <<'\n';

        }
        plik_.close();
        break;
    }
    }while(selection!=5);
    return 0;
}
